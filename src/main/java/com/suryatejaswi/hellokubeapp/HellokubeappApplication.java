package com.suryatejaswi.hellokubeapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class HellokubeappApplication {

	public static void main(String[] args) {
		SpringApplication.run(HellokubeappApplication.class, args);

	}

	@RequestMapping("/")
	public String home() {
		return "Hello Docker World";
	}

}

/**
 *
 *
 * $ kubectl create deployment demo --image=europe-north1-docker.pkg.dev/eng-hash-343715/hellokubeapps/helloworld-springimage --dry-run -o=yaml > deployment.yaml
 * $ echo --- >> deployment.yaml
 * $ kubectl create service clusterip demo --tcp=8080:8080 --dry-run -o=yaml >> deployment.yaml
 */
